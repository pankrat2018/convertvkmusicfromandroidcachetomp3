﻿using System.IO;

namespace ConvertVKMusicFromAndroidCacheToMp3
{
  public class Work
    {
        public static bool Convert(string source, string save, bool delete)
        {
            
            try
            {
                
                    if(!source.Contains(".covers"))
                    {

                    if(delete)
                    {
                        File.Move(source, save + "\\" + Path.GetFileNameWithoutExtension(source) + ".mp3");
                    }
                    else
                    {
                        File.Copy(source, save + "\\" + Path.GetFileNameWithoutExtension(source) + ".mp3");
                    }
                     
                    
                    }
                    else
                    {
                        File.Delete(source);
                    }

                return true;
            }
            catch { return false; }
        }
    }
}
