﻿namespace ConvertVKMusicFromAndroidCacheToMp3
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.textBoxVKFiles = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.textBoxSave = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.buttonStart = new System.Windows.Forms.Button();
            this.checkBoxSave = new System.Windows.Forms.CheckBox();
            this.radioButtonDeleteSource = new System.Windows.Forms.RadioButton();
            this.radioButtonSaveSource = new System.Windows.Forms.RadioButton();
            this.SuspendLayout();
            // 
            // textBoxVKFiles
            // 
            this.textBoxVKFiles.Location = new System.Drawing.Point(13, 27);
            this.textBoxVKFiles.Multiline = true;
            this.textBoxVKFiles.Name = "textBoxVKFiles";
            this.textBoxVKFiles.Size = new System.Drawing.Size(259, 55);
            this.textBoxVKFiles.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 8);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(131, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Путь до файлов из кэша";
            // 
            // textBoxSave
            // 
            this.textBoxSave.Location = new System.Drawing.Point(13, 101);
            this.textBoxSave.Multiline = true;
            this.textBoxSave.Name = "textBoxSave";
            this.textBoxSave.Size = new System.Drawing.Size(259, 55);
            this.textBoxSave.TabIndex = 2;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(13, 85);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(221, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Куда сохранять изменённые файлы (путь)";
            // 
            // buttonStart
            // 
            this.buttonStart.Location = new System.Drawing.Point(13, 233);
            this.buttonStart.Name = "buttonStart";
            this.buttonStart.Size = new System.Drawing.Size(259, 23);
            this.buttonStart.TabIndex = 4;
            this.buttonStart.Text = "Начать работу";
            this.buttonStart.UseVisualStyleBackColor = true;
            this.buttonStart.Click += new System.EventHandler(this.buttonStart_Click);
            // 
            // checkBoxSave
            // 
            this.checkBoxSave.AutoSize = true;
            this.checkBoxSave.Location = new System.Drawing.Point(13, 163);
            this.checkBoxSave.Name = "checkBoxSave";
            this.checkBoxSave.Size = new System.Drawing.Size(171, 17);
            this.checkBoxSave.TabIndex = 5;
            this.checkBoxSave.Text = "Сохранить в исходную папку";
            this.checkBoxSave.UseVisualStyleBackColor = true;
            this.checkBoxSave.CheckedChanged += new System.EventHandler(this.checkBoxSave_CheckedChanged);
            // 
            // radioButtonDeleteSource
            // 
            this.radioButtonDeleteSource.AutoSize = true;
            this.radioButtonDeleteSource.Checked = true;
            this.radioButtonDeleteSource.Location = new System.Drawing.Point(13, 187);
            this.radioButtonDeleteSource.Name = "radioButtonDeleteSource";
            this.radioButtonDeleteSource.Size = new System.Drawing.Size(245, 17);
            this.radioButtonDeleteSource.TabIndex = 6;
            this.radioButtonDeleteSource.TabStop = true;
            this.radioButtonDeleteSource.Text = "Удалять исходные файлы (быстрый метод)";
            this.radioButtonDeleteSource.UseVisualStyleBackColor = true;
            this.radioButtonDeleteSource.CheckedChanged += new System.EventHandler(this.radioButtonDeleteSource_CheckedChanged);
            // 
            // radioButtonSaveSource
            // 
            this.radioButtonSaveSource.AutoSize = true;
            this.radioButtonSaveSource.Location = new System.Drawing.Point(13, 210);
            this.radioButtonSaveSource.Name = "radioButtonSaveSource";
            this.radioButtonSaveSource.Size = new System.Drawing.Size(268, 17);
            this.radioButtonSaveSource.TabIndex = 7;
            this.radioButtonSaveSource.Text = "Сохранять исходные файлы (медленный метод)";
            this.radioButtonSaveSource.UseVisualStyleBackColor = true;
            this.radioButtonSaveSource.CheckedChanged += new System.EventHandler(this.radioButtonSaveSource_CheckedChanged);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 267);
            this.Controls.Add(this.radioButtonSaveSource);
            this.Controls.Add(this.radioButtonDeleteSource);
            this.Controls.Add(this.checkBoxSave);
            this.Controls.Add(this.buttonStart);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.textBoxSave);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.textBoxVKFiles);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "Form1";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Converter";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button buttonStart;
        public System.Windows.Forms.TextBox textBoxVKFiles;
        public System.Windows.Forms.TextBox textBoxSave;
        private System.Windows.Forms.CheckBox checkBoxSave;
        private System.Windows.Forms.RadioButton radioButtonDeleteSource;
        private System.Windows.Forms.RadioButton radioButtonSaveSource;
    }
}

