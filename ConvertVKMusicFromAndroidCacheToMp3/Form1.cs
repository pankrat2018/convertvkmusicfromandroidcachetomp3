﻿using System;
using System.IO;
using System.Threading;
using System.Windows.Forms;

namespace ConvertVKMusicFromAndroidCacheToMp3
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        string _SavePut = "";
        bool delete = true; // для радио кнопок на удаление
        private void buttonStart_Click(object sender, EventArgs e)
        {
            try
            {
                if (textBoxVKFiles.Text.Trim() == "")
                {
                    MessageBox.Show("Вы забыли указать, откуда брать исходные файлы", "", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }
                if (textBoxSave.Text.Trim() == "" && !checkBoxSave.Checked)
                {
                    MessageBox.Show("Вы забыли указать, куда сохранять файлы", "", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }
                if(!textBoxVKFiles.Text.Contains(":\\"))
                {
                    MessageBox.Show("Неправильный путь исходных файлов", "", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
                if (!textBoxSave.Text.Contains(":\\") && !checkBoxSave.Checked)
                {
                    MessageBox.Show("Неправильный путь для сохранения обработанных файлов", "", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }

                if(checkBoxSave.Checked)
                {
                    _SavePut = textBoxVKFiles.Text;
                }
                else
                {
                    _SavePut = textBoxSave.Text;
                }
                string[] file_list = Directory.GetFiles(textBoxVKFiles.Text);
                MessageBox.Show("Дождитесь завершения работы", "", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                Thread t = new Thread(work);
                t.Start(file_list);
               // t.Join();
            }
            catch
            {
                MessageBox.Show("Произошла ошибка", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        private void work(object files)
        {
            string[] file_list = (string[])files;
            int sucess = 0;
            int fail = 0;
            for(int i = 0; i < file_list.Length; i++)
            {
                if(Work.Convert(file_list[i],_SavePut,delete))
                {
                    sucess++;
                }
                else
                {
                    fail++;
                }
            }
            MessageBox.Show("Обработано: " + sucess + " файлов\nОшибок: " + fail, "", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);

        }

        private void checkBoxSave_CheckedChanged(object sender, EventArgs e)
        {
            if(checkBoxSave.Checked)
            {
                textBoxSave.Text = "";
            }
            textBoxSave.Enabled = !checkBoxSave.Checked;
        }

        private void radioButtonDeleteSource_CheckedChanged(object sender, EventArgs e)
        {
            delete = radioButtonDeleteSource.Checked;
        }

        private void radioButtonSaveSource_CheckedChanged(object sender, EventArgs e)
        {
            delete = !radioButtonSaveSource.Checked;
        }
    }
}
