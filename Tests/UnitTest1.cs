﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using ConvertVKMusicFromAndroidCacheToMp3;
using System.IO;

namespace Tests
{
    [TestClass]
    public class UnitTest1
    {
        const string source = "C:\\Users\\Alexandr\\Documents\\Visual Studio 2015\\Projects\\ConvertVKMusicFromAndroidCacheToMp3\\Tests\\TestFiles\\TestMusic"; // путь до модифицируемого файлп
        const string save = "C:\\Users\\Alexandr\\Documents\\Visual Studio 2015\\Projects\\ConvertVKMusicFromAndroidCacheToMp3\\Tests\\TestResult"; // куда сохранять

        [TestMethod]
        public void TestExceptions()
        {
            Assert.IsFalse(Work.Convert("", "",false));
        }

        [TestMethod]
        public void TestWorkSaveSource()
        {
            if(Work.Convert(source, save,false))
            {
                Assert.IsTrue(File.Exists(save + "TestMusic.mp3"));
                Assert.IsTrue(File.Exists(source));
            }
            
        }
        [TestMethod]
        public void TestWorkDeleteSource()
        {
            if (Work.Convert(source, save, true))
            {
                Assert.IsTrue(File.Exists(save + "TestMusic.mp3"));
                Assert.IsFalse(File.Exists(source));
            }

        }
    }
}
